# OpenAI React UI
Open two separated terminal run independently
## _1st Running the Back End_
```sh
cd server
node index.js
```
## _2nd Running the Front End_
```sh
cd chatgpt
npm start
```
## _Launch Browser_
```sh
http://localhost:3000
```